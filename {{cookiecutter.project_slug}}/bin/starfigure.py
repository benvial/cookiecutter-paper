#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
from panflute import *


def star(elem,doc):
    if type(elem) == Image:
        star = "star" in elem.classes
        if not star:
            return
        else:
            try:
                w = elem.attributes["width"]
                width = float((w.split("%")[0]))/100
            except:
                width=1.0
            legend = ""
            for e in elem.content:
                txt = stringify(e)
                if type(e) == Math:
                    txt = "$" + e.text +"$"
                legend += txt
                
            out = r"\begin{figure*}[htbp]"
            out +="\n\centering"
            out +="\n\includegraphics[width="
            out += f"{width}" +" \paperwidth]{" + f"{elem.url}"
            out += "}\n\caption{"
            out += legend + "}"
            if elem.identifier:
                out += "\n\label{" + elem.identifier + "}"
            out += "\n\end{figure*}"
            return RawInline(out,format="latex")


def main(doc=None):
    return run_filter(star,doc=doc)

if __name__ == "__main__":
    main()
