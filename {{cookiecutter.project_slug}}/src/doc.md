

# Introduction {#sec:intro}


## Subsection
\kant[1]

### Subsubsection

A cool citation [@zheludevRoadAheadMetamaterials2010] or that one
[@vialModeCouplingModel2015;@vialEnhancedTunabilityFerroelectric2019;@vialQuasimodalExpansionElectromagnetic2014] 
and a reference to a 
figure Fig. (@fig:1) and an equation Eq. (@eq:1). \kant

![Caption for this figure.](figure1){#fig:1 width=45%}

\kant[1]

$$ 
\begin{aligned} 
  \max_{\rho} \quad & \alpha\,\sigma(E) + (1-\alpha)\,\theta(E)\\ 
  \textrm{s.t.} \quad & f_{\rm min} < f < f_{\rm max}
\end{aligned}
$${#eq:1}

Some more math:

$$ x =1 $${#eq:2}


# Other section {#sec:other}

![Caption for this figure. It is full width!](figure1){#fig:2 width=100% .star}

\kant[4-5]
