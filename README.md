
# Cookiecutter Paper

## Requirements

- a Tex installation (pdflatex, bibtex, packages...)
- bibtool
- python packages (cf. requirements.txt)


## Building

```
make all
```
